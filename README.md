# Mysql operations

This role will automate actions from mysql disaster recovery procedure

##  Requirements

Needs to run on the foreman machine

##  Role Variables

**Global variables: (defaults/main.yml)**

- user: this is the linux user under which commands will be ran on database nodes (default: root)
- backup_dir: the directory to store the backups (default: /backups/mysql )

**Local variables:**

Defined in the playbook that runs the role:

- master: the FQDN of the mysql server that is configured as mysql master

- slave: the FQDN of the mysql server that is configured as mysql slave

- promote: this will run the task to promote the slave server to master (true/false)

- create_slave: this will run the task to create a slave server using a live backup from master (true/false)

- backup: this will perform a full backup of the master and store it in <backup_dir>/mysqlbkp-D-M-Y

  Note:  'promote' and 'create_slave' should not be set both to true in a single run, these tasks should be run separately with manual checks in between.

##  Dependencies

None

##  Example Playbook

```yaml
---
- hosts: db_nodes
  roles:
   - role: mysql-operations
     master: m1-dbs0001.mgmt.oiaas
     slave: m1-dbs0002.mgmt.oiaas
     promote: false
     create_slave: true
     backup: false
```

Run: 

`ansible-playbook $(tools/all-extravars.sh) run.yml -v`